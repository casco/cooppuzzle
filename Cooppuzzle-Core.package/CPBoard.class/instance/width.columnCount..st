initialize
width: requestedBoardWidth columnCount: desiredNumberOfColumns
	| cellSide id |
	desiredNumberOfColumns \\ 4 ~= 0
		ifTrue: [ self error: 'The number of columns should be a multiple of 4' ].
	columnCount := desiredNumberOfColumns.
	rowCount := columnCount // 4 * 3.
	cells := OrderedCollection new.
	cellSide := requestedBoardWidth // columnCount.
	width := cellSide * columnCount.
	height := cellSide * rowCount.
	id := 1.
	1 to: rowCount do: [ :row | 
		1 to: columnCount do: [ :col | 
			cells
				add:
					(CPCell
						id: id
						column: col
						row: row
						side: cellSide).
			id := id + 1 ] ]