testing
intersects: aCPCell
	^ (1 @ 1 extent: (width - 1) @ (height - 1))
		intersects: (aCPCell x @ aCPCell y extent: (aCPCell side - 1) @ (aCPCell side - 1))