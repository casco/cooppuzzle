instance-creation
width: requestedBoardWidth columnCount: desiredNumberOfColumns
	^ self new
		width: requestedBoardWidth columnCount: desiredNumberOfColumns;
		yourself