testing
testImage
	| cell piece image |
	cell := CPCell
		id: 1
		column: 2
		row: 3
		side: 10.
	image := CPGameTest sampleImageOfWidth: 600.
	piece := CPPiece cell: cell onImage: image.
	self assert: piece image bits equals: (image copy: (10@20 extent: 10@10) ) bits