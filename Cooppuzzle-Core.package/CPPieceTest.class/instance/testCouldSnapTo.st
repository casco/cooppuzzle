testing
testCouldSnapTo
	| cell pieceOne |
	cell := CPCell
		id: 1
		column: 2
		row: 3
		side: 10.
	self assert: cell x @ cell y equals: 10 @ 20.
	pieceOne := CPPiece cell: cell onImage: (Form extent: 800 @ 600).
	self assert: pieceOne x @ pieceOne y equals: 10 @ 20.
	self assert: (pieceOne couldSnapTo: cell).
	pieceOne moveTo: cell position - (pieceOne couldSnapTolerance @ pieceOne couldSnapTolerance).
	self assert: (pieceOne couldSnapTo: cell).
	pieceOne moveTo: cell position + (pieceOne couldSnapTolerance @ pieceOne couldSnapTolerance).
	self assert: (pieceOne couldSnapTo: cell).
	pieceOne moveTo: cell position + ((pieceOne couldSnapTolerance + 1) @ pieceOne couldSnapTolerance).
	self deny: (pieceOne couldSnapTo: cell)