testing
testSolve
	| cell piece image |
	cell := CPCell
		id: 1
		column: 2
		row: 3
		side: 10.
	image := Form extent: 10 @ 10.
	piece := CPPiece cell: cell onImage: image.
	piece moveTo: 15 @ 15.
	self deny: piece solved.
	piece solve.
	self assert: piece solved