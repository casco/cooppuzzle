testing
testSnapTo
	| cell pieceOne pieceTwo |
	cell := CPCell
		id: 1
		column: 2
		row: 3
		side: 10.
	pieceOne := CPPiece cell: cell onImage: (Form extent: 10 @ 10).
	pieceTwo := CPPiece cell: cell onImage: (Form extent: 10 @ 10).
	pieceOne snapTo: cell.
	self assert: pieceOne x @ pieceOne y equals: cell x @ cell y.
	pieceOne snapTo: pieceTwo.
	self assert: pieceOne x @ pieceOne y equals: pieceTwo x @ pieceTwo y