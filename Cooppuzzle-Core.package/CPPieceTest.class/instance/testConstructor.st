testing
testConstructor
	| cell piece image |
	cell := CPCell
		id: 1
		column: 2
		row: 3
		side: 10.
	image := CPGameTest sampleImageOfWidth: 800.
	piece := CPPiece cell: cell onImage: image.
	self assert: piece cell equals: cell.
	self assert: piece image bits equals: (image copy: (Rectangle origin: 10 @ 20 extent: 10 @ 10)) bits.
	self assert: piece solved.
	self assert: piece x equals: cell x.
	self assert: piece y equals: cell y