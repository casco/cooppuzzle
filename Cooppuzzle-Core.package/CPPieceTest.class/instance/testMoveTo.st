testing
testMoveTo
	| cell piece image ann |
	cell := CPCell
		id: 1
		column: 2
		row: 3
		side: 10.
	image := Form extent: 800 @ 600.
	piece := CPPiece cell: cell onImage: image.
	piece announcer when: PieceMovedAnnouncement send: #pieceMovedAnnouncement: to: self.
	piece moveTo: 15 @ 15.
	self assert: announcements size equals: 1.
	ann := announcements last.
	self assert: ann from equals: 10 @ 20.
	self assert: ann to equals: 15 @ 15.
	self assert: ann piece equals: piece.
	self assert: piece position equals: 15 @ 15.
 	piece moveTo: 16 @ 16.
	self assert: announcements size equals: 2.
 	piece moveTo: 16 @ 16.
	self assert: announcements size equals: 2.
	
