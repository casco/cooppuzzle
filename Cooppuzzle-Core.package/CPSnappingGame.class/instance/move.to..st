playing
move: piece to: position
	| cellToSnapTo |
	piece moveTo: position.
	cellToSnapTo := board cells
		detect: [ :cell | piece couldSnapTo: cell withTolerance: couldSnapTreshold ]
		ifNone: [ nil ].
	cellToSnapTo
		ifNotNil: [piece moveTo: cellToSnapTo position ]