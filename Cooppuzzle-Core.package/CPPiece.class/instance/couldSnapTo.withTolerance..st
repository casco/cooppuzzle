testing
couldSnapTo: aCPCell withTolerance: tolerance
	^ self position
		between: aCPCell position - (tolerance @ tolerance)
		and: aCPCell position + (tolerance @ tolerance)