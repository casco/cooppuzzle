moving
moveTo: aPoint
	| ann |
	self position = aPoint
		ifTrue: [ ^ self ].
	ann := PieceMovedAnnouncement for: self from: x @ y to: aPoint.
	x := aPoint x.
	y := aPoint y.
	announcer announce: ann