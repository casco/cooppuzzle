initialization
cell: aCPCell onImage: aForm
	cell := aCPCell.
	baseImage := aForm.
	x := cell x.
	y := cell y.
	announcer := Announcer new.
	image := baseImage copy: (Rectangle origin: x @ y corner: x @ y + (cell side @ cell side))