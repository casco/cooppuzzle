testing
testImageWidthColumnsSmallerThanBoard
	| game|
	game := CPGame new.
	self should: [ game image: (Form extent: 600 @ 450) width: 700 columns: 4 ] raise: Error.
