testing
testImageWidthColumnsPerfectFit
	| game image imageSix |
	game := CPGame new.
	image := Form extent: 600 @ 450.
	game image: image width: 600 columns: 4.
	self assert: game pieces size equals: 12.
	imageSix := image copy: (Rectangle origin: 151 @ 151 extent: 150 @ 150).
	self assert: (game pieces at: 6) image bits equals: imageSix bits