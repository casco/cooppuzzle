testing
testShufflePieces
	| game image  |
	game := CPGame new.
	image := Form extent: 800 @ 600.
	game image: image width: 800 columns: 4.
	game shufflePieces.
	game pieces anySatisfy: [ :each | each solved not ]
