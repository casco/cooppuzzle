testing
testImageWidthColumns
	| game image imageSix pieceSix |
	game := CPGame new.
	image := self class sampleImageOfWidth: 600.
	self should: [ game image: image width: 700 columns: 4 ] raise: Error.
	game image: image width: 600 columns: 4.
	self assert: game pieces size equals: 12.
	pieceSix := game pieces at: 6.
	self assert: pieceSix position equals: 150 @ 150.
	imageSix := image copy: (Rectangle origin: 150 @ 150 extent: 150 @ 150).
	self assert: pieceSix image bits equals: imageSix bits