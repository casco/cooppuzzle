data
sampleImageOfWidth: width
	| height parent |
	width \\ 4 > 0
		ifTrue: [ self error: 'width should be multiple of 4' ].
	height := width / 4 * 3.
	parent := Morph  newBounds: (0 @ 0 extent: width @ height) color: Color white.
	0 to: width - 10 by: 10 do: [ :x | 
		0 to: height - 10 by: 10 do: [ :y | 
			parent
				addMorph:
					(Morph
						newBounds: (x @ y extent: 10 @ 10)
						color: (Color r: 254 atRandom g: 254 atRandom b: 254 atRandom)) ] ].
	^ parent imageForm