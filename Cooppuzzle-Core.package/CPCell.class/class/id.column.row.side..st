instance creation
id: anId column: columnNumber row: rowNumber side: sideSize
	^ self new
		id: anId
			column: columnNumber
			row: rowNumber
			side: sideSize;
		yourself