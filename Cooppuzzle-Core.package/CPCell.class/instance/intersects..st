testing
intersects: aCPCell
	^ (self x @ self y extent: (side - 1) @ side - 1)
		intersects: (aCPCell x @ aCPCell y extent: (aCPCell side - 1) @ aCPCell side - 1)