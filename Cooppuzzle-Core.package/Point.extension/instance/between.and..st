*Cooppuzzle-Core
between: minPoint and: maxPoint
	^ minPoint <= self & (self <= maxPoint)