preparing
image: aForm width: width columns: anInteger
	board := CPBoard width: width columnCount: anInteger.
	aForm extent = board extent
		ifFalse: [ self error: 'Image dimensions should match board dimentions' ].
	self image: aForm.