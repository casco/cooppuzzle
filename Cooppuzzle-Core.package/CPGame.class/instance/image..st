preparing
image: aForm
	pieces isEmpty
		ifTrue: [ pieces := board cells collect: [ :each | CPPiece cell: each onImage: aForm ] ]
		ifFalse: [ pieces do: [ :each | each baseImage: aForm ] ]