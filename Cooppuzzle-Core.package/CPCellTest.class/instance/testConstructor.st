testing
testConstructor
	| cell |
	cell := CPCell
		id: 1
		column: 1
		row: 1
		side: 5.
	self assert: cell id equals: 1.
	self assert: cell x equals: 0.
	self assert: cell y equals: 0.
	self assert: cell position  equals: 0@0.
	self assert: cell side equals: 5.
	cell := CPCell
		id: 1
		column: 2
		row: 2
		side: 5.
	self assert: cell id equals: 1.
	self assert: cell x equals: 5.
	self assert: cell y equals: 5.
	self assert: cell side equals: 5