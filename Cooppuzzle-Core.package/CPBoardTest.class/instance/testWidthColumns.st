testing
testWidthColumns
	| board cellOne |
	board := CPBoard width: 800 columnCount: 4.
	self assert: board width equals: 800.
	self assert: board height equals: 600.
	self assert: board rowCount equals: 3.
	self assert: board columnCount equals: 4.
	cellOne := board cells at: 1. 
	self assert: cellOne position equals: 0 @ 0. 
	self assert: cellOne  side equals: 200.
	board := CPBoard width: 803 columnCount: 4.
	self assert: board width equals: 800.
	self assert: board height equals: 600.
	self assertCellsOkInBoard: board.
	self should: [ CPBoard width: 600 columnCount: 5 ] raise: Error