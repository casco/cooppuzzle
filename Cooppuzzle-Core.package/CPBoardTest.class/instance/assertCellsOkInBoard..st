testing
assertCellsOkInBoard: board
	self assert: board cells size equals: 12.
	self assertCollection: (board cells collect: [ :each | each id ]) equals: (1 to: 12) asOrderedCollection.
	self assert: board cells first id equals: 1.
	self assert: board cells first column equals: 1.
	self assert: board cells first row equals: 1.
	self assert: (board cells at: 6) id equals: 6.
	self assert: (board cells at: 6) column equals: 2.
	self assert: (board cells at: 6) row equals: 2.
	self assert: board cells last id equals: 12.
	self assert: board cells last column equals: 4.
	self assert: board cells last row equals: 3.
	self assert: (board cells sum: [ :each | each side * each side ]) equals: board height * board width.
	self assert: board cells first x @ board cells first y equals: 0 @ 0.
	self assert: (board cells at: 6) x @ (board cells at: 6) y equals: 200 @ 200.
	self assert: board cells last x @ board cells last y equals: 600 @ 400.
	self assert: (board cells allSatisfy: [ :each | each side = (board width // board columnCount) ]).
	board cells
		do: [ :cell | 
			self deny: ((board cells copyWithout: cell) anySatisfy: [ :each | each intersects: cell ]).
			self assert: (board intersects: cell) ]