initialization
initializePresenter
	fromFileButton action: [ self newGameFromFile ].
	destroyButton action: [ game destroy ].
	shuffleButton action: [ game shuffle ].
	solveButton action: [ game solve ]