actions
newGameFromFile
	self chooseImageFileName.
	imageFile notNil
		ifTrue: [ self chooseColumnCount.
			columns notNil
				ifTrue: [ game := CPMorphicGame
						image: (Form fromFileNamed: imageFile)
						columns: columns
						offset: 50 @ 50.
					imageFile := nil.
					columns := nil ] ]