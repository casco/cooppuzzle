initialization
initializeWidgets
	fromFileButton := self newButton label: 'New'.
	destroyButton := self newButton label: 'Destroy'.
	shuffleButton := self newButton label: 'Shuffle'.
	solveButton := self newButton label: 'Solve'