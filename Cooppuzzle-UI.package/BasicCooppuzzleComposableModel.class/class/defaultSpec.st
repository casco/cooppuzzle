specs
defaultSpec
	^ SpecLayout composed
		newColumn: [ :column | 
			column
				add: #fromFileButton;
				add: #shuffleButton;
				add: #solveButton;
				add: #destroyButton ];
		yourself