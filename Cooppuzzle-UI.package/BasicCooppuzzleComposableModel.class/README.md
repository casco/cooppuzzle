a basic spec component to create, and manage coppuzzles.

BasicCooppuzzleComposableModel new openWithSpec ..

| button  | 
button := PluggableButtonMorph
		newButtonFor: BasicCooppuzzleComposableModel new
		action: #openWithSpec
		label: 'Cooppuzzle'
		help: 'Open the Cooppuzzle menu'.
button openInWorld 