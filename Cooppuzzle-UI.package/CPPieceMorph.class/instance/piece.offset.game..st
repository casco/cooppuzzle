accessing
piece: aPiece offset: anOffset game: aGame
	piece := aPiece.
	offset := anOffset.
	game := aGame.
	piece announcer when: PieceMovedAnnouncement send: #pieceMovedAnnouncement: to: self.
	self position: piece position + offset.
	self form: aPiece image.
	self beUnsticky 