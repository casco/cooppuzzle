accessing
piece: aPiece offset: anOffset
	piece := aPiece.
	offset := anOffset.
	piece announcer when: PieceMovedAnnouncement send: #pieceMovedAnnouncement: to: self.
	self position: (piece position + offset).
	self form: aPiece image