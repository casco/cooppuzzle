announcements
pieceMovedAnnouncement: ann
	ann piece position + offset ~= self position
		ifTrue: [ self position: ann piece position + offset.
			Transcript
				show: '          -> ' , ann piece position printString;
				cr ]