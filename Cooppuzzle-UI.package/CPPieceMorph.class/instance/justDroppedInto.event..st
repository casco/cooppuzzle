moving
justDroppedInto: aMorph event: anEvent
   super justDroppedInto: aMorph event: anEvent.
	Transcript
		show: piece position printString , ' -> ' , (self position - offset) printString;
		cr.
	piece position ~= (self position - offset)
		ifTrue: [ game move: piece to: self position - offset ].
