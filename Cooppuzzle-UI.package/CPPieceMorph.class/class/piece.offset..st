instance-creation
piece: aPiece offset: anOffset
	^ self new
		piece: aPiece offset: anOffset;
		yourself