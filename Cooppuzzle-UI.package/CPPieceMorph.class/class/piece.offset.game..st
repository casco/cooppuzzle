instance-creation
piece: aPiece offset: anOffset game: aGame
	^ self new
		piece: aPiece offset: anOffset game: aGame;
		yourself