initialization
board: aCPBoard offset: aPoint 
	board := aCPBoard .
	offset := aPoint.
	self position: aPoint.
	self color: Color veryLightGray .
	self lock: true.
	self extent: board width @ board height 