A morphic app for the Cooppuzzle game.

(World  submorphs select: [ :each | each class =  CPPieceMorph]) do: [ :each | World removeMorph: each ]