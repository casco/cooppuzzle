actions
destroy
	pieceMorphs do: [ :each | each world removeMorph: each  ].
	boardMorph world removeMorph: boardMorph 