initialization
image: aForm columns: cols offset: aPoint

	game := CPSnappingGame image: aForm width: aForm width columns: cols.
	offset := aPoint.
	boardMorph := CPBoardMorph board: game board offset: self offset.
	boardMorph openInWorld.
	pieceMorphs := game pieces
		collect: [ :each | CPPieceMorph piece: each offset: self offset game: game].
	pieceMorphs do: [ :each | each openInWorld ]